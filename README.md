# RabbitMQ Examples Implemented in Java with Swing

This project contains some RabbitMQ examples in Java and using Swing for the UI.

## Background

I wanted to learn a bit about RabbitMQ and demonstrate that I understood the
basic principles of the Publish/Subscribe pattern and that I could code against 
it.  Then I decided I wanted to have a little fun with it and do something 
visually interesting, so I put this idea into action.

The concept that I'm playing with here is the idea of a book publisher sending
products out to warehouses.  I also want to implement an invoicing system that
can publish invoices and have multiple nodes pick them up for processing.  And
all the while, stick some graphs and tables into the works to make it fun to
look at (and fun to code.)

## What You'll Need

### Building This Project

I wrote and built this code using the following:

* An internet connection
* Java 15
* Maven 3.6.3
* RabbitMQ 3.8.17

Earlier versions of the software might work fine.

To build the project, first clone, cd into the project directory, and run Maven:

    mvn clean package
    
This project is configured to build a *fat jar*.  When the build process completes
successfully, you'll want to use the jar named `swingmq-1.0-SNAPSHOT-jar-with-dependencies.jar`.

## Running This Project

These instructions assume one is running Linux.

To run the project, you'll need the "-with-dependencies" jar that's built as a
result of the Maven build process (found in the target directory) and a running
instance of RabbitMQ with default settings.  (The sample applications here all
expect to find RabbitMQ on the localhost and at port 5672.)

Once RabbitMQ is running and you've built the project, you can run all of the 
sample applications by typing

    source runall
    
You can invoke individual applications by sourcing the individual app scripts.
For example:

    source publisher
    source warehouse
    
### Running RabbitMQ in Docker

Assuming you have Docker installed and that you're on Linux, you can start 
RabbitMQ with default settings as follows:

    sudo docker run -d --hostname my-rabbit --rm -p 5672:5672 --name managed_rabbit rabbitmq:3-management
    
I've found this to be the easiest way to get RabbitMQ installed and running.

## Usage

The way I envisaged this project being used was to start the Publisher and then
start some number of Warehouses.  Hit the "send one" button on the publisher
to send a single order to one of the warehouses, or hit the green "automate" 
button to start sending orders every 500 milliseconds or so.  Use the up and 
down arrows to adjust the speed.  You can see the buttons I'm talking about
in the screenshots below.

What you should see when you have these things running is the publisher logging
the orders it's sending, and one of the warehouses receiving and "processing"
the order.


## Screenshots

![SwingPublisher](images/swingpublisher-screenshot.png)
![SwingWarehouse](images/swingwarehouse-screenshot.png)


