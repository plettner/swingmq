package com.sodapopsoftware.mq.model;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class ProductFactoryTest {

	private final ProductFactory productFactory = new ProductFactory();

	@Test
	@DisplayName("Create random book product")
	public void createBook() {
		for (int index = 0; index < 1000; index++) {
			final Product product = this.productFactory.create();
			assertNotNull(product);
			// System.out.println(product);
		}
	}
}
