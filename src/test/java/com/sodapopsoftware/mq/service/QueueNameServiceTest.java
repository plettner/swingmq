package com.sodapopsoftware.mq.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class QueueNameServiceTest {

	@Test
	void getName1() {
		final String expected = "WarehouseChannel1";
		final QueueNameService queueNameService = new QueueNameService();
		final String actual = queueNameService.getName(1);
		Assertions.assertEquals(expected, actual);
	}

	@Test
	void getName10() {
		final String expected = "WarehouseChannel10";
		final QueueNameService queueNameService = new QueueNameService();
		final String actual = queueNameService.getName(10);
		Assertions.assertEquals(expected, actual);
	}

}
