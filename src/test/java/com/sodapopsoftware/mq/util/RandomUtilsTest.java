package com.sodapopsoftware.mq.util;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.sodapopsoftware.mq.model.BookTag;
import com.sodapopsoftware.mq.util.RandomUtils;

class RandomUtilsTest {

	private static final int MAX_ITERATIONS = 1000;

	private final RandomUtils randomUtils = new RandomUtils();

	@Test
	@DisplayName("Create a number from zero to max")
	public void createFromZeroTo() {
		final int expectedMax = 9;
		final int frequency[] = new int[expectedMax + 1];
		for (int index = 0; index < MAX_ITERATIONS; index++) {
			final int actual = this.randomUtils.createFromZeroTo(expectedMax);
			frequency[actual]++;
			assertTrue(actual <= expectedMax);
		}
		/*
		 * for (int index = 0; index < frequencies.length; index++) { System.out.println("" + index + " : "
		 * + frequencies[index]); }
		 */
	}

	@Test
	@DisplayName("Create a number from min to max")
	public void createFromMinToMax() {
		final int expectedMin = 100;
		final int expectedMax = 199;
		for (int index = 0; index < MAX_ITERATIONS; index++) {
			final int actual = this.randomUtils.createBetween(expectedMin, expectedMax);
			assertTrue(actual >= expectedMin);
			assertTrue(actual <= expectedMax);
			// System.out.println(actual);
		}
	}

	@Test
	@DisplayName("Create a random price")
	public void createPrice() {
		for (int index = 0; index < MAX_ITERATIONS; index++) {
			final float actual = this.randomUtils.createPrice(10, 19);
			assertTrue(actual >= 10.0f && actual < 20.00f, "Value should be 10.00 <= x < 20");
			// System.out.println(actual);
		}
	}

	@Test
	@DisplayName("Create a lower case letter")
	public void createLowerCaseLetter() {
		final char actual = this.randomUtils.createLowerCaseLetter();
		assertTrue(Character.isLowerCase(actual), "Should be lower case");
	}

	@Test
	@DisplayName("Create an upper case letter")
	public void createUpperCaseLetter() {
		final char actual = this.randomUtils.createUpperCaseLetter();
		assertTrue(Character.isUpperCase(actual));
	}

	@Test
	@DisplayName("Create a random capitalized word")
	public void createCapitalizedWord() {
		final int expectedMaxLength = 10;
		for (int index = 0; index < MAX_ITERATIONS; index++) {
			final String actual = this.randomUtils.createCapitalizedWord(expectedMaxLength);
			assertNotNull(actual);
			assertTrue(actual.length() <= expectedMaxLength,
					"Should be fewer than " + expectedMaxLength + " characters");
			assertTrue(actual.length() > 0, "Should be nonzero length");
			assertTrue(Character.isUpperCase(actual.charAt(0)), "First letter should be upper case");
		}
	}

	@Test
	@DisplayName("Create a random lowercase word")
	public void createLowercaseWord() {
		final int expectedMaxLength = 10;
		for (int index = 0; index < MAX_ITERATIONS; index++) {
			final String actual = this.randomUtils.createWord(expectedMaxLength);
			assertNotNull(actual);
			assertTrue(actual.length() <= expectedMaxLength,
					"Should be fewer than " + expectedMaxLength + " characters");
			assertTrue(actual.length() > 0, "Should be nonzero length");
			assertTrue(Character.isLowerCase(actual.charAt(0)), "First letter should be lower case");
		}
	}

	@Test
	@DisplayName("Create a random product title")
	public void createTitle() {
		for (int index = 0; index < MAX_ITERATIONS; index++) {
			final String actual = this.randomUtils.createTitle();
			assertNotNull(actual);
			assertTrue(actual.length() > 0);
			// System.out.println(actual);
		}
	}

	@Test
	@DisplayName("Create an author name")
	public void createAuthorName() {
		for (int index = 0; index < MAX_ITERATIONS; index++) {
			final String actual = this.randomUtils.createAuthorName();
			assertNotNull(actual);
			assertTrue(actual.length() > 0);
			// System.out.println(actual);
		}
	}

	@Test
	@DisplayName("Create book tag")
	public void createBookTag() {
		for (int index = 0; index < MAX_ITERATIONS; index++) {
			final BookTag actual = this.randomUtils.createBookTag();
			assertNotNull(actual);
		}
	}

	@Test
	@DisplayName("Create book tag list")
	public void createBookTagList() {
		for (int index = 0; index < MAX_ITERATIONS; index++) {
			final List<BookTag> actual = this.randomUtils.createBookTagList();
			assertNotNull(actual);
			assertTrue(actual.size() >= 1);
		}
	}
}
