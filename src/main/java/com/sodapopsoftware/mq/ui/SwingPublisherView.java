package com.sodapopsoftware.mq.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import com.sodapopsoftware.mq.model.Order;

public class SwingPublisherView extends JPanel {

	private static final long serialVersionUID = 8285746245426381385L;

	private JTable jTable;
	private final ToolBar toolBar = new ToolBar();
	private final OrderTableModel tableModel = new OrderTableModel();

	public SwingPublisherView() {
	}

	/**
	 * Set up and start the view
	 */
	public void go() {

		setLayout(new BorderLayout());

		add(this.toolBar.getJToolBar(), BorderLayout.NORTH);

		this.jTable = new JTable(this.tableModel);
		this.jTable.setPreferredScrollableViewportSize(new Dimension(500, 70));

		// Create the scroll pane and add the table to it.
		final JScrollPane scrollPane = new JScrollPane(this.jTable);

		// Add the scroll pane to this panel.
		add(scrollPane);

	}

	/**
	 * Tell the toolbar that someone wants to listen to toolbar actions like a button press
	 *
	 * @param toolBarListener an object that wants to listen to toolbar actions
	 */
	public void addListener(ToolBarListener toolBarListener) {
		this.toolBar.addListener(toolBarListener);
	}

	/**
	 * Let the view know that an order has been sent out to warehouses and that it should be displayed
	 * in the view.
	 *
	 * @param order an order that was generated and published
	 */
	public void add(Order order) {
		this.tableModel.add(order);
		this.jTable.revalidate();
	}
}
