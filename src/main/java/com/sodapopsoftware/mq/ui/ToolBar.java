package com.sodapopsoftware.mq.ui;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToolBar;

import com.sodapopsoftware.mq.AppException;

public class ToolBar implements ActionListener {

	private enum ActionType {
		AUTOMATE, STOP, SINGLE, SPEED_UP, SLOW_DOWN;
	}

	private JButton buttonStart;
	private JButton buttonStop;
	private JButton buttonSingle;
	private JButton buttonSlowDown;
	private JButton buttonSpeedUp;
	private final JToolBar jToolBar;

	private final List<ToolBarListener> toolBarListeners = new LinkedList<>();

	public ToolBar() {
		this.jToolBar = init();
	}

	public JToolBar getJToolBar() {
		return this.jToolBar;
	}

	private JButton addToolBarImageButton(final String toolTip, final String imageName) {
		final URL imageUrl = this.getClass().getResource(imageName);
		final ImageIcon icon = new ImageIcon(imageUrl, toolTip);
		final JButton b = new JButton(icon);
		b.setMargin(new Insets(0, 0, 0, 0));
		b.setToolTipText(toolTip);
		b.setActionCommand("Toolbar:" + imageName);
		b.addActionListener(this);
		return b;
	}

	public JToolBar init() {
		final JToolBar toolBar = new JToolBar();

		this.buttonStart = addToolBarImageButton("Automate Order Sending", "/SwingPublisher/images/tbStart.gif");
		this.buttonStop = addToolBarImageButton("Pause Order Sending", "/SwingPublisher/images/tbPause.gif");
		this.buttonSingle = addToolBarImageButton("Send One Order", "/SwingPublisher/images/tbAddThread.gif");
		this.buttonSpeedUp = addToolBarImageButton("Speed Up", "/SwingPublisher/images/tbIncDelay.gif");
		this.buttonSlowDown = addToolBarImageButton("Slow Down", "/SwingPublisher/images/tbDecDelay.gif");
		toolBar.add(this.buttonStart);
		toolBar.add(this.buttonStop);
		toolBar.addSeparator();
		toolBar.add(this.buttonSpeedUp);
		toolBar.add(this.buttonSlowDown);
		toolBar.addSeparator();
		toolBar.add(this.buttonSingle);

		return toolBar;
	}

	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		ActionType actionType = null;
		final Object o = actionEvent.getSource();

		if (o == this.buttonStart) {
			actionType = ActionType.AUTOMATE;
		} else if (o == this.buttonStop) {
			actionType = ActionType.STOP;
		} else if (o == this.buttonSingle) {
			actionType = ActionType.SINGLE;
		} else if (o == this.buttonSpeedUp) {
			actionType = ActionType.SPEED_UP;
		} else if (o == this.buttonSlowDown) {
			actionType = ActionType.SLOW_DOWN;
		} else {
			throw new AppException("Unknown button press");
		}

		notifyEveryone(actionType);
	}

	public void addListener(ToolBarListener toolBarListener) {
		this.toolBarListeners.add(toolBarListener);
	}

	private void notifyEveryone(ActionType actionType) {
		for (final ToolBarListener toolBarListener : this.toolBarListeners) {
			switch (actionType) {
			case AUTOMATE:
				toolBarListener.automateOrders();
				break;
			case STOP:
				toolBarListener.stopOrders();
				break;
			case SINGLE:
				toolBarListener.singleOrder();
				break;
			case SPEED_UP:
				toolBarListener.speedUp();
				break;
			case SLOW_DOWN:
				toolBarListener.slowDown();
				break;
			}
		}
	}

}
