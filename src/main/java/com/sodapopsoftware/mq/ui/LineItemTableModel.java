package com.sodapopsoftware.mq.ui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingConstants;
import javax.swing.table.AbstractTableModel;

import com.sodapopsoftware.mq.model.LineItem;
import com.sodapopsoftware.mq.model.Order;

public class LineItemTableModel extends AbstractTableModel {

	private static final long serialVersionUID = -2782303269487740836L;

	private final String[] columnNames = { "Order #", "ISBN", "Title", "Quantity", "Unit Price", "Total Price" };
	private final List<OrderLineItem> tableRows = new ArrayList<>();

	public LineItemTableModel() {
	}

	@Override
	public int getColumnCount() {
		return this.columnNames.length;
	}

	@Override
	public int getRowCount() {
		return this.tableRows.size();
	}

	@Override
	public String getColumnName(int col) {
		return this.columnNames[col];
	}

	public int getDefaultColumnAlignment(int colunnNumber) {
		if (colunnNumber == 1 || colunnNumber == 2) {
			return SwingConstants.LEFT;
		} else {
			return SwingConstants.RIGHT;
		}
	}

	@Override
	public Object getValueAt(int row, int col) {
		Object returnValue = "--Error--";
		if (row < getRowCount()) {
			final Order order = this.tableRows.get(row).getOrder();
			final LineItem lineItem = this.tableRows.get(row).getLineItem();

			switch (col) {
			case 0:
				returnValue = order.getOrderNumber();
				break;
			case 1:
				returnValue = lineItem.getProduct().getISBN();
				break;
			case 2:
				returnValue = lineItem.getProduct().getTitle();
				break;
			case 3:
				returnValue = lineItem.getQuantity();
				break;
			case 4:
				returnValue = String.format("%.2f", lineItem.getProduct().getPrice());
				break;
			case 5:
				returnValue = String.format("%.2f", lineItem.getSubTotal());
				break;
			}
		}
		return returnValue;
	}

	/**
	 * JTable uses this method to determine the default renderer/ editor for each cell. If we didn't
	 * implement this method, then the last column would contain text ("true"/"false"), rather than a
	 * check box.
	 */
	@Override
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	/**
	 * Adds a product to this table so it can be displayed
	 *
	 * @param product a product that our system ships from one place to another
	 */
	public void add(Order order, LineItem lineItem) {
		this.tableRows.add(new OrderLineItem(order, lineItem));
	}

	/**
	 * Convenience class that connects a lineItem to a given order. I'm doing this because Orders and
	 * LineItems are immutable and I don't wanna break that property. But to add an Order to a LineItem
	 * at some point after creating the LineItem object would break immutability. Because I only need to
	 * connect the two while here in this table model, I'll have this little convenience class do that
	 * for me.
	 */
	class OrderLineItem {
		private final Order order;
		private final LineItem lineItem;

		public OrderLineItem(Order order, LineItem lineItem) {
			this.order = order;
			this.lineItem = lineItem;
		}

		public Order getOrder() {
			return this.order;
		}

		public LineItem getLineItem() {
			return this.lineItem;
		}
	}
}
