package com.sodapopsoftware.mq.ui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;

import com.sodapopsoftware.mq.SwingWarehouse;
import com.sodapopsoftware.mq.model.Book;
import com.sodapopsoftware.mq.model.DataUpdate;
import com.sodapopsoftware.mq.model.DataUpdateListener;
import com.sodapopsoftware.mq.model.DataUpdateType;
import com.sodapopsoftware.mq.model.LineItem;
import com.sodapopsoftware.mq.model.Order;
import com.sodapopsoftware.mq.model.Product;

/**
 * This is the view for our "warehouse" application. It consists of a boss-ware pie chart and a
 * table of information about the orders we have received.
 */
public class SwingWarehouseView extends JSplitPane implements DataUpdateListener {

	private static final long serialVersionUID = 7305516557206101068L;

	private JPanel panelTop;
	private JPanel panelBottom;
	private final LineItemTable lineItemTable = new LineItemTable();
	private final BookPieDataset pieDataSet = new BookPieDataset();

	public SwingWarehouseView() {
	}

	public void go() {

		this.panelTop = new PieChartPanel(this.pieDataSet);
		this.panelTop.setOpaque(true);
		this.panelTop.setSize(SwingWarehouse.PANEL_WIDTH, SwingWarehouse.PANEL_HEIGHT);

		this.panelBottom = new JPanel();
		this.panelBottom.setLayout(new BorderLayout());
		this.panelBottom.add(this.lineItemTable, BorderLayout.CENTER);
		this.panelBottom.setOpaque(true);
		this.panelBottom.setSize(SwingWarehouse.PANEL_WIDTH, SwingWarehouse.PANEL_HEIGHT);

		setOrientation(SwingConstants.HORIZONTAL);
		setTopComponent(this.panelTop);
		setBottomComponent(this.panelBottom);
		setOneTouchExpandable(true);
		setDividerLocation(SwingWarehouse.PANEL_HEIGHT);
	}

	/**
	 * Invoked whenever an order is received and our display needs updating.
	 *
	 * @param dataUpdate information about the data update
	 */
	@Override
	public void dataUpdated(DataUpdate dataUpdate) {

		if (dataUpdate.getDataUpdateType() != DataUpdateType.ORDER || this.pieDataSet == null) {
			return;
		}

		final Order order = (Order) dataUpdate.getData();
		for (final LineItem lineItem : order) {
			final Product product = lineItem.getProduct();
			if (product instanceof Book) {
				final Book book = (Book) product;
				final int quantity = lineItem.getQuantity();
				final Number number = this.pieDataSet.getIntValue(book.getBookTag().getName());
				final int additional = number == null ? 0 : number.intValue();
				this.pieDataSet.setValue(book.getBookTag().getName(), quantity + additional);
			}

			this.lineItemTable.add(order, lineItem);

		}

		// We updated our data, so force Swing to update the chart
		revalidate();
		this.lineItemTable.revalidate();
		this.lineItemTable.repaint();
		repaint();
	}

}
