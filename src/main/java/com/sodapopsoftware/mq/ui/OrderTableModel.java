package com.sodapopsoftware.mq.ui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.sodapopsoftware.mq.model.Order;

public class OrderTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 40386320243981477L;

	private final String[] columnNames = { "Warehouse #", "Order #", "Item Count", "Total Cost" };
	private final List<Order> tableRows = new ArrayList<>();

	@Override
	public int getColumnCount() {
		return this.columnNames.length;
	}

	@Override
	public int getRowCount() {
		return this.tableRows.size();
	}

	@Override
	public String getColumnName(int col) {
		return this.columnNames[col];
	}

	@Override
	public Object getValueAt(int row, int col) {
		Object returnValue = "--Error--";
		if (row < getRowCount()) {
			final Order order = this.tableRows.get(row);

			switch (col) {
			case 0:
				returnValue = order.getWarehouseNumber();
				break;
			case 1:
				returnValue = order.getOrderNumber();
				break;
			case 2:
				returnValue = order.getItemCount();
				break;
			case 3:
				returnValue = order.getTotalCost();
				break;
			}
		}
		return returnValue;
	}

	/**
	 * JTable uses this method to determine the default renderer/ editor for each cell. If we didn't
	 * implement this method, then the last column would contain text ("true"/"false"), rather than a
	 * check box.
	 */
	@Override
	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	public void add(Order order) {
		this.tableRows.add(order);
	}

}
