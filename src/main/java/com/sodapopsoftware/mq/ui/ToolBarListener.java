package com.sodapopsoftware.mq.ui;

public interface ToolBarListener {

	/**
	 * Send a single order
	 */
	void singleOrder();

	/**
	 * Automate the sending of orders
	 */
	void automateOrders();

	/**
	 * Ends the automated sending of orders
	 */
	void stopOrders();

	/**
	 * Reduces the delay between automated order publishes.
	 */
	void speedUp();

	/**
	 * Increases the delay between automated order publishes.
	 */
	void slowDown();

}
