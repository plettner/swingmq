package com.sodapopsoftware.mq.ui;

import org.jfree.data.UnknownKeyException;
import org.jfree.data.general.DefaultPieDataset;

public class BookPieDataset extends DefaultPieDataset<String> {

	private static final long serialVersionUID = -1080656551697728008L;

	public BookPieDataset() {
	}

	public int getIntValue(String key) {
		try {
			final Number number = this.getValue(key);
			return number.intValue();
		} catch (final UnknownKeyException exception) {
			return 0;
		}
	}

}
