package com.sodapopsoftware.mq.ui;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.Locale;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;

/**
 * Encapsulates the drawing of a pretty pie chart that renders our color counts
 */
public class PieChartPanel extends JPanel {

	private static final long serialVersionUID = -6308715582822701286L;

	private final BookPieDataset dataset;
	private final boolean withLegend = false;
	private final boolean withToolTips = true;
	private static final String CHART_TITLE = "Books Ordered by Type";
	private JFreeChart chart;
	private PlotOrientation plotOrientation;

	public PieChartPanel(BookPieDataset dataset) {
		super();
		this.dataset = dataset;
		init(PlotOrientation.VERTICAL);
	}

	public PlotOrientation getPlotOrientation() {
		return this.plotOrientation;
	}

	public void setPlotOrientation(PlotOrientation plotOrientation) {
		this.plotOrientation = plotOrientation;
	}

	void init(PlotOrientation plotOrientation) {

		setPlotOrientation(plotOrientation);

		if (this.chart == null) {
			// Since 1.5.x, it's suggested to use Orson-Charts or something.
			this.chart = ChartFactory.createPieChart3D(CHART_TITLE, this.dataset, this.withLegend, this.withToolTips,
					Locale.US);
		}
	}

	@Override
	public void paint(Graphics graphics) {
		// Draw the pie chart graphics onto the panel's drawing space
		final Graphics2D graphics2D = (Graphics2D) graphics;
		final Rectangle2D chartArea = getVisibleRect();
		this.chart.draw(graphics2D, chartArea, null, null);
	}

}
