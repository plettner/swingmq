package com.sodapopsoftware.mq.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import com.sodapopsoftware.mq.model.LineItem;
import com.sodapopsoftware.mq.model.Order;

public class LineItemTable extends JPanel {

	private static final long serialVersionUID = 3419172045192300400L;

	private JTable jTable;
	private final LineItemTableModel tableModel = new LineItemTableModel();

	public LineItemTable() {
		init();
	}

	private void init() {
		this.jTable = new JTable(this.tableModel);
		this.jTable.setPreferredScrollableViewportSize(new Dimension(500, 70));
		final JScrollPane scrollPane = new JScrollPane(this.jTable);
		setLayout(new BorderLayout());
		add(scrollPane, BorderLayout.CENTER);
		setOpaque(true);

		DefaultTableCellRenderer renderer = null;
		final TableColumn col = null;
		for (int index = 0; index < this.tableModel.getColumnCount(); index++) {
			renderer = new DefaultTableCellRenderer();
			renderer.setHorizontalAlignment(this.tableModel.getDefaultColumnAlignment(index));
			this.jTable.getColumnModel().getColumn(index).setCellRenderer(renderer);
		}
	}

	public JTable getJTable() {
		return this.jTable;
	}

	public void add(Order order, LineItem lineItem) {
		this.tableModel.add(order, lineItem);
		this.jTable.revalidate();
		this.jTable.repaint();
	}

}
