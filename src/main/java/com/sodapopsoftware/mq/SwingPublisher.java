package com.sodapopsoftware.mq;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import com.sodapopsoftware.mq.model.Order;
import com.sodapopsoftware.mq.model.OrderFactory;
import com.sodapopsoftware.mq.service.OrderPublishingService;
import com.sodapopsoftware.mq.ui.SwingPublisherView;
import com.sodapopsoftware.mq.ui.ToolBarListener;
import com.sodapopsoftware.mq.util.ArgumentUtil;
import com.sodapopsoftware.mq.util.RandomUtils;

/**
 * Main method for publishing orders to the "warehouse" app via RabbitMQ
 */
public class SwingPublisher extends JFrame implements ToolBarListener {

	private static final long serialVersionUID = 6112518921350076123L;

	public final static String APP_NAME = "SwingPublisher";

	public static final int PANEL_HEIGHT = 300;
	public static final int PANEL_WIDTH = 400;

	public static final int LOCATION_X = 50;
	public static final int LOCATION_Y = 100;

	private final OrderFactory orderFactory = new OrderFactory();
	private OrderPublishingService orderPublishingServices;
	private final RandomUtils randomUtils = new RandomUtils();

	public static SwingPublisher swingPublisher;
	public SwingPublisherView swingPublisherView;

	private AutomatedPublishingThread automatedPublishingThread;

	private int maxWarehouses = ArgumentUtil.DEFAULT_MAX_WAREHOUSES;
	private int xOffset = ArgumentUtil.DEFAULT_OFFSET_HORIZONTAL;
	private int yOffset = ArgumentUtil.DEFAULT_OFFSET_VERTICAL;

	public static void main(String[] arguments) {

		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (final Exception IGNORED) {
			// This exception is ignored
		}
		JFrame.setDefaultLookAndFeelDecorated(true);

		final ArgumentUtil argumentUtils = new ArgumentUtil(arguments);
		final int maxWarehouses = argumentUtils.getMaxNumberOfWarehouses();
		final int xOffset = argumentUtils.getHorizontalOffset();
		final int yOffset = argumentUtils.getVerticalOffset();

		swingPublisher = new SwingPublisher(maxWarehouses, xOffset, yOffset);
		swingPublisher.go();
	}

	public SwingPublisher(int maxWarehouses, int xOffset, int yOffset) {
		this.maxWarehouses = maxWarehouses;
		this.xOffset = xOffset;
		this.yOffset = yOffset;
	}

	public int getMaxWarehouses() {
		return this.maxWarehouses;
	}

	public void go() {

		setTitle(APP_NAME);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		this.swingPublisherView = new SwingPublisherView();
		this.swingPublisherView.addListener(this);

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				SwingPublisher.this.swingPublisherView.go();
			}
		});

		setSize(PANEL_WIDTH, PANEL_HEIGHT * 2);
		setLocation(LOCATION_X + this.xOffset, LOCATION_Y + this.yOffset);
		this.add(this.swingPublisherView);
		setVisible(true);

		this.orderPublishingServices = new OrderPublishingService(this.maxWarehouses);
	}

	@Override
	public void singleOrder() {
		final int warehouseNumber = this.randomUtils.createBetween(1, this.maxWarehouses);
		final Order order = this.orderFactory.create(warehouseNumber);
		this.orderPublishingServices.publish(warehouseNumber, order);
		this.swingPublisherView.add(order);
	}

	/**
	 * Starts a thread that repeatedly publishes orders until stopped by the stopOrders() method.
	 */
	@Override
	public void automateOrders() {
		if (this.automatedPublishingThread == null) {
			this.automatedPublishingThread = new AutomatedPublishingThread();
			this.automatedPublishingThread.start();
		}
	}

	@Override
	public void speedUp() {
		if (this.automatedPublishingThread != null) {
			this.automatedPublishingThread.decrementDelay();
		}
	}

	@Override
	public void slowDown() {
		if (this.automatedPublishingThread != null) {
			this.automatedPublishingThread.incrementDelay();
		}
	}

	/**
	 * Stops the automated publishing thread, if present.
	 */
	@Override
	public void stopOrders() {
		if (this.automatedPublishingThread != null) {
			this.automatedPublishingThread.stopRunning();
			this.automatedPublishingThread = null;
		}
	}

	public class AutomatedPublishingThread extends Thread {

		private boolean running = true;
		private int delayInMilliseconds = 500;

		public boolean isRunning() {
			return this.running;
		}

		public void stopRunning() {
			this.running = false;
		}

		public int getDelayInMilliseconds() {
			return this.delayInMilliseconds;
		}

		public void setDelayInMilliseconds(int delayInMilliseconds) {
			this.delayInMilliseconds = delayInMilliseconds;
		}

		public void incrementDelay() {
			this.delayInMilliseconds += 100;
		}

		public void decrementDelay() {
			this.delayInMilliseconds -= 100;
			if (this.delayInMilliseconds < 100) {
				this.delayInMilliseconds = 100;
			}
		}

		@Override
		public void run() {
			while (isRunning()) {
				singleOrder();
				try {
					sleep(getDelayInMilliseconds());
				} catch (final InterruptedException IGNORED) {
					// We'll receive this exception when the thread is stopped
				}
			}
		}

	}

}
