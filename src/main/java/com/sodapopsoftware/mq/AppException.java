package com.sodapopsoftware.mq;
import java.io.Serializable;

public class AppException extends RuntimeException implements Serializable {

	private static final long serialVersionUID = 7604969833946878633L;

	public AppException(Throwable throwable) {
		super(throwable);
	}

	public AppException(String message) {
		super(message);
	}

	public AppException(String message, Throwable throwable) {
		super(message, throwable);
	}

}