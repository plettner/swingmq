package com.sodapopsoftware.mq.service;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.SerializationUtils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.sodapopsoftware.mq.AppException;
import com.sodapopsoftware.mq.model.Order;

/**
 * This service "publishes" orders to the RabbitMQ service for distribution to all interested
 * parties.
 */
public class OrderPublishingService {

	final QueueNameService queueNameService = new QueueNameService();

	final ConnectionFactory connectionFactory = new ConnectionFactory();
	private Connection connection = null;
	private final Channel[] channels;
	private final int maxQueues;

	public OrderPublishingService(int maxQueues) {
		if (maxQueues <= 0) {
			throw new AppException("Can't have fewer than one MQ queues; was " + maxQueues);
		}
		this.maxQueues = maxQueues;
		this.channels = new Channel[maxQueues];

		createPublisherQueues();
	}

	private Channel getChannel(int warehouseNumber) {
		if (!isValidWarehouseNumber(warehouseNumber)) {
			throw new AppException("Invalid warehouseNumber " + warehouseNumber);
		}

		return this.channels[warehouseNumber - 1];
	}

	private void setChannel(int warehouseNumber, Channel channel) {
		if (!isValidWarehouseNumber(warehouseNumber)) {
			throw new AppException("Invalid warehouseNumber " + warehouseNumber);
		}

		this.channels[warehouseNumber - 1] = channel;
	}

	private boolean isValidWarehouseNumber(int warehouseNumber) {
		return warehouseNumber >= 1 || warehouseNumber < this.maxQueues;
	}

	/**
	 * Forms all of our connections, channels, and queues
	 */
	private void createPublisherQueues() {

		this.connectionFactory.setHost("localhost");

		try {
			this.connection = this.connectionFactory.newConnection();
		} catch (IOException | TimeoutException exception) {
			throw new AppException("Could not create a new connection", exception);
		}

		// Note that warehouse numbers are one-based, and not zero-based like our queues
		for (int warehouseNumber = 1; warehouseNumber <= this.maxQueues; warehouseNumber++) {

			try {
				setChannel(warehouseNumber, this.connection.createChannel());
			} catch (final IOException exception) {
				throw new AppException("Failed to create a new channel", exception);
			}

			try {
				final String queueName = this.queueNameService.getName(warehouseNumber);
				getChannel(warehouseNumber).queueDeclare(queueName, false, false, false, null);
			} catch (final IOException exception) {
				throw new AppException("Failed establishing the queue", exception);
			}

		}
	}

	/**
	 * Publishes an order to the message queue.
	 *
	 * @param order an order of products
	 */
	public void publish(int warehouseNumber, Order order) {

		final byte[] orderBytes = SerializationUtils.serialize(order);

		try {
			final String queueName = this.queueNameService.getName(warehouseNumber);
			getChannel(warehouseNumber).basicPublish("", queueName, null, orderBytes);
		} catch (final IOException exception) {
			throw new AppException("Failed to successfully publish an order", exception);
		}

	}

}
