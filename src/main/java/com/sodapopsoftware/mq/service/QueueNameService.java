package com.sodapopsoftware.mq.service;

public class QueueNameService {

	private static final String WAREHOUSE_CHANNEL_FORMAT = "WarehouseChannel%d";

	public String getName(int warehouseNumber) {
		return String.format(WAREHOUSE_CHANNEL_FORMAT, warehouseNumber);
	}

}
