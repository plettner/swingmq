package com.sodapopsoftware.mq.service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.SerializationUtils;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.Delivery;
import com.sodapopsoftware.mq.AppException;
import com.sodapopsoftware.mq.model.DataUpdate;
import com.sodapopsoftware.mq.model.DataUpdateListener;
import com.sodapopsoftware.mq.model.DataUpdateType;
import com.sodapopsoftware.mq.model.Order;

/**
 * This service is responsible for listening to the RabbitMQ service and collecting anything sent to
 * this "warehouse" on the "hello" queue.
 */
public class WarehouseReceivingService implements DeliverCallback, CancelCallback {

	final ConnectionFactory connectionFactory = new ConnectionFactory();
	private Connection connection;
	private Channel channel;
	private final QueueNameService queueNameService = new QueueNameService();

	List<DataUpdateListener> listeners = new LinkedList<>();

	public WarehouseReceivingService() {
	}

	/**
	 * Create this receiving service and specify something that will be updated when this receiver
	 * receives something of interest.
	 *
	 * @param dataUpdateListener a object that will be notified when the receiver receives something
	 */
	public WarehouseReceivingService(DataUpdateListener dataUpdateListener) {
		addListener(dataUpdateListener);
	}

	/**
	 * Adds an object that wants to be notified of this receivers activities.
	 *
	 * @param dataUpdateListener an object that will be notified of this receiver's activities
	 */
	public void addListener(DataUpdateListener dataUpdateListener) {
		this.listeners.add(dataUpdateListener);
	}

	/**
	 * Notifies all of the listeners of this receiver's activities.
	 *
	 * @param order an order that was received here and that should be sent to all listeners
	 */
	private void notifyEveryone(Order order) {
		for (final DataUpdateListener dataUpdateListener : this.listeners) {
			final DataUpdate dataUpdate = new DataUpdate(DataUpdateType.ORDER, order);
			dataUpdateListener.dataUpdated(dataUpdate);
		}
	}

	/**
	 * Starts up this warehouse receiver service by forming a connection and a channel for listening to
	 * the RabbitMQ service. Throws AppExceptions upon unrecoverable errors.
	 */
	public void start(String hostName, int warehouseNumber) {
		this.connectionFactory.setHost(hostName);

		try {
			this.connection = this.connectionFactory.newConnection();
		} catch (IOException | TimeoutException exception) {
			throw new AppException("Could not create connection", exception);
		}

		try {
			this.channel = this.connection.createChannel();
		} catch (final IOException exception) {
			throw new AppException("Problem creating channel", exception);
		}

		final String queueName = this.queueNameService.getName(warehouseNumber);
		try {
			this.channel.queueDeclare(queueName, false, false, false, null);
		} catch (final IOException exception) {
			throw new AppException("Error trying to create queue", exception);
		}

		try {
			this.channel.basicConsume(queueName, true, this, this);
		} catch (

		final IOException exception) {
			throw new AppException("basicConsume failed", exception);
		}

	}

	/**
	 * Callback routine that processes the incoming messages from RabbitMQ. For this app, we
	 * de-serialized the data, and then, depending on the type of object sent to us, we deal with it
	 * accordingly.
	 * <p>
	 * The original example code uses lambdas to configure the consume routine. I chose not to because
	 * you can't re-use that kind of code, nor can you write tests for it.
	 *
	 * @param consumerTag
	 * @param delivery    the data that was sent to use
	 */
	@Override
	public void handle(String consumerTag, Delivery delivery) throws IOException {
		final Object object = SerializationUtils.deserialize(delivery.getBody());
		if (object instanceof Order) {
			final Order order = (Order) object;
			notifyEveryone(order);
		}
	}

	@Override
	public void handle(String consumerTag) throws IOException {
		throw new AppException("CancelBack invoked; I don't know what to do here");
	}

}
