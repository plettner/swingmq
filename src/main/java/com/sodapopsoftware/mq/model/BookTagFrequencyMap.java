package com.sodapopsoftware.mq.model;

import java.util.HashMap;
import java.util.Map;

public class BookTagFrequencyMap {

	private final Map<String, BookTagFrequency> map = new HashMap<>();

	public BookTagFrequencyMap() {
	}

	public void increment(BookTag bookTag, int amount) {
		BookTagFrequency bookTagFrequency = this.map.get(bookTag.toString());
		if (bookTagFrequency == null) {
			bookTagFrequency = new BookTagFrequency(bookTag, amount);
		} else {
			bookTagFrequency.setCount(bookTagFrequency.getCount() + amount);
		}
		this.map.put(bookTag.toString(), bookTagFrequency);
	}

	public int getCount(BookTag bookTag) {
		final BookTagFrequency bookTagFrequency = this.map.get(bookTag.toString());
		if (bookTagFrequency != null) {
			return bookTagFrequency.getCount();
		}
		return 0;
	}
}
