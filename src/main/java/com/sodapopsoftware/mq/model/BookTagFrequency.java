package com.sodapopsoftware.mq.model;

public class BookTagFrequency {

	private BookTag bookTag;
	private int count;

	public BookTagFrequency(BookTag bookTag, int count) {
		this.bookTag = bookTag;
		this.count = count;
	}

	public BookTag getBookTag() {
		return this.bookTag;
	}

	public void setBookTag(BookTag bookTag) {
		this.bookTag = bookTag;
	}

	public int getCount() {
		return this.count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
