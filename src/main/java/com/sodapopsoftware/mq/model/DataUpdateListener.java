package com.sodapopsoftware.mq.model;

public interface DataUpdateListener {

	/**
	 * Invoked to notify this implementer that there is a data update
	 *
	 * @param dataUpdate contains information about the data update
	 */
	void dataUpdated(DataUpdate dataUpdate);

}
