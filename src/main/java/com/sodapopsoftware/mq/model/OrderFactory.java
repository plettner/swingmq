package com.sodapopsoftware.mq.model;

import com.sodapopsoftware.mq.util.RandomUtils;

public class OrderFactory {

	private final RandomUtils randomUtils = new RandomUtils();
	private final LineItemFactory lineItemFactory = new LineItemFactory();

	public OrderFactory() {
	}

	public Order create(int warehouseNumber) {
		final int NumberOfLineItems = this.randomUtils.createBetween(1, 10);

		final Order.OrderBuilder orderBuilder = new Order.OrderBuilder();
		orderBuilder.warehouseNumber(warehouseNumber);
		orderBuilder.orderNumber(this.randomUtils.createBetween(1000000, 1999999));
		for (int index = 0; index < NumberOfLineItems; index++) {
			orderBuilder.lineItem(this.lineItemFactory.create());
		}

		return orderBuilder.build();
	}
}
