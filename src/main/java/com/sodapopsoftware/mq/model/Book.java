package com.sodapopsoftware.mq.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.sodapopsoftware.mq.AppException;

/**
 * An immutable Book product.
 */
public class Book extends Product {

	private static final long serialVersionUID = 8288932797781485011L;

	private final String author;
	private final int pageCount;

	/** Yeah, books can belong to multiple genres, but that just muddies this demo */
	private final BookTag bookTag;

	private Book(BookBuilder bookBuilder) {
		super(bookBuilder.isbn, bookBuilder.title, bookBuilder.price);
		this.author = bookBuilder.author;
		this.pageCount = bookBuilder.pageCount;
		this.bookTag = bookBuilder.bookTag;
	}

	public String getAuthor() {
		return this.author;
	}

	public int getPageCount() {
		return this.pageCount;
	}

	public BookTag getBookTag() {
		return this.bookTag;
	}

	@Override
	public String toString() {
		final String s = new ToStringBuilder(this).append("ISBN", this.isbn).append("Title", this.title)
				.append("Author", this.author).append("Price", this.price).append("Pages", this.pageCount)
				.append("BookTag", this.bookTag).toString();
		return s;
	}

	/**
	 * This builder class is able to construct an immutable Book object.
	 */
	public static class BookBuilder {

		private String isbn;
		private String title;
		private String author;
		private float price;
		private int pageCount;
		private BookTag bookTag;

		public BookBuilder() {
		}

		public BookBuilder(String isbn, String title, String author, float price, int pageCount, BookTag bookTag) {
			this.isbn = isbn;
			this.title = title;
			this.author = author;
			this.price = price;
			this.pageCount = pageCount;
			this.bookTag = bookTag;
		}

		public BookBuilder isbn(String isbn) {
			this.isbn = isbn;
			return this;
		}

		public BookBuilder title(String title) {
			this.title = title;
			return this;
		}

		public BookBuilder author(String author) {
			this.author = author;
			return this;
		}

		public BookBuilder price(float price) {
			this.price = price;
			return this;
		}

		public BookBuilder pageCount(int pageCount) {
			this.pageCount = pageCount;
			return this;
		}

		public BookBuilder bookTag(BookTag bookTag) {
			this.bookTag = bookTag;
			return this;
		}

		public Book build() {
			if (this.isbn == null || this.title == null || this.author == null || this.pageCount <= 0
					|| this.price <= 0.0 || this.bookTag == null) {
				throw new AppException("Missing parameters");
			}
			return new Book(this);
		}
	}

}
