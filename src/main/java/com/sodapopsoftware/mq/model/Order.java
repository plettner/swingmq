package com.sodapopsoftware.mq.model;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.sodapopsoftware.mq.AppException;

/**
 * An immutable Order object, containing "line items" of some quantity of product
 */
public class Order implements Serializable, Iterable<LineItem> {

	private static final long serialVersionUID = -575188258094997934L;

	private final int warehouseNumber;
	private final int orderNumber;
	private final List<LineItem> lineItems;

	private Order(int warehouseNumber, int orderNumber, List<LineItem> lineItems) {
		this.warehouseNumber = warehouseNumber;
		this.orderNumber = orderNumber;
		this.lineItems = lineItems;
	}

	public int getWarehouseNumber() {
		return this.warehouseNumber;
	}

	public int getOrderNumber() {
		return this.orderNumber;
	}

	public LineItem get(int index) {
		return this.lineItems.get(index);
	}

	@Override
	public Iterator<LineItem> iterator() {
		return this.lineItems.iterator();
	}

	public int getItemCount() {
		int itemCount = 0;
		for (final LineItem lineItem : this) {
			itemCount += lineItem.getQuantity();
		}
		return itemCount;
	}

	public float getTotalCost() {
		float totalCost = 0.0f;
		for (final LineItem lineItem : this) {
			totalCost += lineItem.getSubTotal();
		}
		return totalCost;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("#", this.orderNumber).append("LineItems", this.lineItems).toString();
	}

	/**
	 * Builder pattern implementation that constructs immutable Order objects
	 */
	public static class OrderBuilder {

		private int warehouseNumber;
		private int orderNumber;
		private final List<LineItem> lineItems = new LinkedList<>();

		public OrderBuilder() {
		}

		public OrderBuilder warehouseNumber(int warehouseNumber) {
			this.warehouseNumber = warehouseNumber;
			return this;
		}

		public OrderBuilder orderNumber(int orderNumber) {
			this.orderNumber = orderNumber;
			return this;
		}

		public OrderBuilder lineItem(LineItem lineItem) {
			this.lineItems.add(lineItem);
			return this;
		}

		public Order build() {
			if (this.warehouseNumber == 0 || this.lineItems.size() == 0 || this.orderNumber == 0) {
				throw new AppException("Order is missing a number or has zero line items");
			}
			return new Order(this.warehouseNumber, this.orderNumber, this.lineItems);
		}

	}
}
