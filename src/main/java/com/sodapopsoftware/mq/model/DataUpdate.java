package com.sodapopsoftware.mq.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class DataUpdate {

	private final DataUpdateType dataUpdateType;
	private final Object data;

	public DataUpdate(DataUpdateType dataUpdateType, Object object) {
		this.dataUpdateType = dataUpdateType;
		this.data = object;
	}

	public DataUpdateType getDataUpdateType() {
		return this.dataUpdateType;
	}

	public Object getData() {
		return this.data;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("Type", this.dataUpdateType).append("data", this.data).toString();
	}

}
