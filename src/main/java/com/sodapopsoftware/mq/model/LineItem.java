package com.sodapopsoftware.mq.model;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Immutable LineItem object, used in creating orders of many products
 */
public class LineItem implements Serializable {

	private static final long serialVersionUID = -5343491697538367925L;

	private final Order enclosingOrder = null;
	private final Product product;
	private final int quantity;

	private LineItem(Product product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}

	public Product getProduct() {
		return this.product;
	}

	public int getQuantity() {
		return this.quantity;
	}

	/**
	 * Computes and returns the subtotal for this line item
	 *
	 * @return the subtotal price for this line item
	 */
	public float getSubTotal() {
		if (getProduct() != null) {
			return getProduct().getPrice() * getQuantity();
		}
		return 0.0f;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("ISBN", getProduct().getISBN()).append("Title", getProduct().getTitle())
				.append("PricePer", getProduct().getPrice()).append("Quantity", this.quantity)
				.append("Subtotal", getSubTotal()).toString();
	}

	/**
	 * Implementation of a builder pattern for constructing immutable LineItem objects
	 */
	public static class LineItemBuilder {

		private Product product = null;
		private int quantity = 0;;

		public LineItemBuilder() {
		}

		public LineItemBuilder product(Product product) {
			this.product = product;
			return this;
		}

		public LineItemBuilder quantity(int quantity) {
			this.quantity = quantity;
			return this;
		}

		public LineItem build() {
			if (this.product == null || this.quantity == 0) {
				throw new IllegalArgumentException("Can't create a LineItem without a product or with zero quantity");
			}
			return new LineItem(this.product, this.quantity);
		}
	}

}
