package com.sodapopsoftware.mq.model;

import java.io.Serializable;

/**
 * Base abstract class for all products this company sells.
 */
public abstract class Product implements Serializable {

	private static final long serialVersionUID = 8906215460707000642L;

	protected final String isbn;
	protected final String title;
	protected final float price;

	protected Product(String isbn, String title, float price) {
		this.isbn = isbn;
		this.title = title;
		this.price = price;
	}

	public String getISBN() {
		return this.isbn;
	}

	public String getTitle() {
		return this.title;
	}

	public float getPrice() {
		return this.price;
	}

}
