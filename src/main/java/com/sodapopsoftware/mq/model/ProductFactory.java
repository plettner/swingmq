package com.sodapopsoftware.mq.model;

import com.sodapopsoftware.mq.util.RandomUtils;

public class ProductFactory {

	private final RandomUtils randomUtils = new RandomUtils();

	/**
	 * Creates a random product.
	 *
	 * @return a randomly created product
	 */
	public Product create() {
		return createBook();
	}

	private Book createBook() {
		final Book.BookBuilder builder = new Book.BookBuilder().isbn(createISBN()).title(createTitle())
				.author(createAuthor()).price(createPrice()).pageCount(createPageCount()).bookTag(createBookTag());
		return builder.build();
	}

	private String createISBN() {
		return String.format("%d-%d-%d-%d", this.randomUtils.createBetween(1, 1000),
				this.randomUtils.createBetween(1, 1000), this.randomUtils.createBetween(1, 1000),
				this.randomUtils.createBetween(1, 1000));
	}

	private String createTitle() {
		return this.randomUtils.createTitle();
	}

	private String createAuthor() {
		return this.randomUtils.createAuthorName();
	}

	private float createPrice() {
		return this.randomUtils.createPrice(8, 30);
	}

	private int createPageCount() {
		return this.randomUtils.createBetween(150, 400);
	}

	private BookTag createBookTag() {
		return this.randomUtils.createBookTag();
	}

}
