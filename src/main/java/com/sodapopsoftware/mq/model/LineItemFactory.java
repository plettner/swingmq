package com.sodapopsoftware.mq.model;

import com.sodapopsoftware.mq.util.RandomUtils;

public class LineItemFactory {

	private final RandomUtils randomUtils = new RandomUtils();
	private final ProductFactory productFactory = new ProductFactory();

	public LineItemFactory() {
	}

	private static final int[] QUANTITIES = { 1, 5, 10, 25, 50, 100 };

	public LineItem create() {
		final Product product = this.productFactory.create();
		final int quantityIndex = this.randomUtils.createFromZeroTo(5);
		return new LineItem.LineItemBuilder().product(product).quantity(QUANTITIES[quantityIndex]).build();
	}
}
