package com.sodapopsoftware.mq.model;

import java.awt.Color;

import com.sodapopsoftware.mq.util.RandomUtils;

public enum BookTag {

	FICTION(Color.BLUE), NON_FICTION(Color.RED), HUMOR(Color.YELLOW), BIOGRAPHY(Color.GREEN), GRAPHIC_NOVEL(Color.CYAN),
	ROMANCE(Color.BLACK), CHILDRENS(Color.LIGHT_GRAY), HISTORY(Color.GRAY), DIY(Color.MAGENTA), GARDENING(Color.ORANGE);

	private Color color;

	BookTag(Color color) {
		this.color = color;
	}

	/**
	 * Convenience method to return the number of book tags defined.
	 *
	 * @return number of book tags defined
	 */
	public static int count() {
		return values().length;
	}

	public static BookTag random() {
		final int randomIndex = new RandomUtils().createBetween(1, count());
		int index = 0;
		for (final BookTag bookTag : values()) {
			if (index == randomIndex) {
				return bookTag;
			}
			index++;
		}
		return FICTION;
	}

	/**
	 * Returns a color for this enum.
	 *
	 * @return a color for this enum.
	 */
	public Color getColor() {
		return this.color;
	}

	/**
	 * Returns the name of this enum.
	 *
	 * @return the name of this enum.
	 */
	public String getName() {
		return name();
	}

}
