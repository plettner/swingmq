package com.sodapopsoftware.mq;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import com.sodapopsoftware.mq.service.WarehouseReceivingService;
import com.sodapopsoftware.mq.ui.SwingWarehouseView;
import com.sodapopsoftware.mq.util.ArgumentUtil;

public class SwingWarehouse extends JFrame {

	private static final long serialVersionUID = 8406198841956820308L;

	public static final String MQ_HOST_NAME = "localhost";

	public static final String APP_NAME = "SwingWarehouse";

	public static final int PANEL_HEIGHT = 300;
	public static final int PANEL_WIDTH = 400;

	public static final int LOCATION_X = 500;
	public static final int LOCATION_Y = 100;

	public static void main(String[] arguments) {

		// Get the relevant command line arguments
		final ArgumentUtil argumentUtils = new ArgumentUtil(arguments);
		final int xOffset = argumentUtils.getHorizontalOffset();
		final int yOffset = argumentUtils.getVerticalOffset();
		final int warehouseNumber = argumentUtils.getWarehouseNumber();

		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (final Exception IGNORED) {
			// This exception is ignored
		}
		JFrame.setDefaultLookAndFeelDecorated(true);

		final SwingWarehouse swingWarehouse = new SwingWarehouse();
		swingWarehouse.go(warehouseNumber, xOffset, yOffset);
	}

	public void go(int warehouseNumber, int xOffset, int yOffset) {

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setTitle(getAppName(warehouseNumber));

		final SwingWarehouseView view = new SwingWarehouseView();

		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				view.go();
			}
		});

		setContentPane(view);

		this.setSize(PANEL_WIDTH, PANEL_HEIGHT * 2);
		setLocation(LOCATION_X + xOffset, LOCATION_Y + yOffset);
		setVisible(true);

		// Start pulling messages from the queue
		new WarehouseReceivingService(view).start(MQ_HOST_NAME, warehouseNumber);
	}

	private String getAppName(int warehouseNumber) {
		return APP_NAME + " #" + warehouseNumber;
	}
}
