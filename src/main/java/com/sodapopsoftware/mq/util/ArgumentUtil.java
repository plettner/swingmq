package com.sodapopsoftware.mq.util;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.sodapopsoftware.mq.AppException;

public class ArgumentUtil {

	public static final String ARG_MAX_WAREHOUSES = "h";
	public static final String ARG_WAREHOUSE = "w";
	public static final String ARG_OFFSET_HORIZONTAL = "x";
	public static final String ARG_OFFSET_VERTICAL = "y";

	public static final int DEFAULT_MAX_WAREHOUSES = 1;
	public static final int DEFAULT_OFFSET_HORIZONTAL = 0;
	public static final int DEFAULT_OFFSET_VERTICAL = 0;
	public static final int DEFAULT_WAREHOUSE_NUMBER = 1;

	private final Options options = new Options();
	private CommandLineParser commandLineParser;
	private CommandLine commandLine;

	public ArgumentUtil(String[] arguments) {
		init(arguments);
	}

	private void init(String[] arguments) {

		this.options.addOption(ARG_MAX_WAREHOUSES, "maxWarehouses", true, "Maximum number of Warehouses (default=1)");
		this.options.addOption(ARG_WAREHOUSE, "warehouse", true, "Warehouse Number (default=0");
		this.options.addOption(ARG_OFFSET_HORIZONTAL, "horizontalOffset", true,
				"Horizontal screen location offset (default=0)");
		this.options.addOption(ARG_OFFSET_VERTICAL, "verticalOffset", true,
				"Vertical screen location offset (default=0)");

		this.commandLineParser = new DefaultParser();
		try {
			this.commandLine = this.commandLineParser.parse(this.options, arguments);
		} catch (final ParseException exception) {
			throw new AppException("Problem parsing command line options", exception);
		}
	}

	public int getHorizontalOffset() {
		return convertToInt(this.commandLine.getOptionValue(ARG_OFFSET_HORIZONTAL), DEFAULT_OFFSET_HORIZONTAL);
	}

	public int getVerticalOffset() {
		return convertToInt(this.commandLine.getOptionValue(ARG_OFFSET_VERTICAL), DEFAULT_OFFSET_VERTICAL);
	}

	public int getWarehouseNumber() {
		return convertToInt(this.commandLine.getOptionValue(ARG_WAREHOUSE), DEFAULT_WAREHOUSE_NUMBER);
	}

	public int getMaxNumberOfWarehouses() {
		return convertToInt(this.commandLine.getOptionValue(ARG_MAX_WAREHOUSES), DEFAULT_MAX_WAREHOUSES);
	}

	private int convertToInt(String s, int defaultValue) {
		if (s == null || s.strip().length() == 0) {
			return defaultValue;
		}

		try {
			return Integer.parseInt(s.strip());
		} catch (final NumberFormatException exception) {
			throw new AppException("Command line argument was expected to be an integer: " + s, exception);
		}
	}

}
