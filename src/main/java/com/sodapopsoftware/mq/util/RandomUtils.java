package com.sodapopsoftware.mq.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.sodapopsoftware.mq.model.BookTag;

/**
 * Simple little random number routines
 */
public class RandomUtils {

	private static final Random RANDOM = new Random();

	public RandomUtils() {
	}

	public int createFromZeroTo(int max) {
		return (int) Math.floor(Math.random() * (max + 1));
	}

	public int createBetween(int min, int max) {
		return (int) Math.floor(Math.random() * (max - min + 1) + min);
	}

	/**
	 * Creates a price value as a float that ends in either .95 or .99
	 *
	 * @return a float that ends in either .95 or .99
	 */
	public float createPrice(int min, int max) {
		final float cents = createFromZeroTo(1) == 0 ? 0.95f : 0.99f;
		final float dollars = createBetween(min, max);
		return dollars + cents;
	}

	public char createLowerCaseLetter() {
		return (char) (RANDOM.nextInt(26) + 'a');
	}

	public char createUpperCaseLetter() {
		return (char) (RANDOM.nextInt(26) + 'A');
	}

	public String createWord(int max) {
		final StringBuilder sb = new StringBuilder();
		if (max > 1) {
			final int randomMax = createBetween(1, max);
			for (int index = 0; index < randomMax; index++) {
				sb.append(createLowerCaseLetter());
			}
		}
		return sb.toString();

	}

	public String createCapitalizedWord(int max) {
		final StringBuilder sb = new StringBuilder();
		sb.append(createUpperCaseLetter());
		if (max > 1) {
			final int randomMax = createFromZeroTo(max - 1);
			for (int index = 0; index < randomMax; index++) {
				sb.append(createLowerCaseLetter());
			}
		}
		return sb.toString();
	}

	public String createTitle() {
		final int MaxWords = 8;
		final int MaxWordLength = 10;
		final int numWords = createBetween(1, MaxWords); // has to be at least 1
		final StringBuilder sb = new StringBuilder();
		sb.append(createCapitalizedWord(MaxWordLength));
		for (int index = 0; index < numWords - 1; index++) {
			sb.append(' ');
			sb.append(createCapitalizedWord(MaxWordLength));
		}
		return sb.toString();
	}

	public String createAuthorName() {
		final StringBuilder sb = new StringBuilder();
		sb.append(createCapitalizedWord(12)).append(' ');
		switch (createBetween(1, 3)) {
		case 1:
			sb.append(createCapitalizedWord(8));
			break;
		case 2:
			sb.append(createUpperCaseLetter()).append('.');
		default:
			// No middle name or initial for this person
			break;
		}
		sb.append(' ').append(createCapitalizedWord(12));
		return sb.toString();
	}

	public BookTag createBookTag() {
		return BookTag.random();
	}

	public List<BookTag> createBookTagList() {
		final List<BookTag> list = new LinkedList<>();
		final int Max = createBetween(1, 5);
		for (int index = 0; index < Max; index++) {
			list.add(createBookTag());
		}
		return list;
	}

}
