package com.sodapopsoftware.mq.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.sodapopsoftware.mq.util.ArgumentUtil;

class ArgumentrUtilTest {
 
	@Test
	@DisplayName("Verify that default values are returned when no arguments supplied")
	public void checkDefaultValues() {
		final String[] ARGUMENTS = { "programName" };
		final ArgumentUtil argumentUtil = new ArgumentUtil(ARGUMENTS);
		Assertions.assertEquals(argumentUtil.getWarehouseNumber(), ArgumentUtil.DEFAULT_WAREHOUSE_NUMBER);
		Assertions.assertEquals(argumentUtil.getHorizontalOffset(), ArgumentUtil.DEFAULT_OFFSET_HORIZONTAL);
		Assertions.assertEquals(argumentUtil.getVerticalOffset(), ArgumentUtil.DEFAULT_OFFSET_VERTICAL);
	}

	@Test
	@DisplayName("Verify that command line values are returned when provided")
	public void checkSuppliedValues() {
		final int offsetHoriz = 10;
		final int offsetVert = 100;
		final int warehouseNumber = 50;
		final String[] ARGUMENTS = { "programName", "-x", "" + offsetHoriz, "-y", "" + offsetVert, "-w",
				"" + warehouseNumber };
		final ArgumentUtil argumentUtil = new ArgumentUtil(ARGUMENTS);
		Assertions.assertEquals(warehouseNumber, argumentUtil.getWarehouseNumber());
		Assertions.assertEquals(offsetHoriz, argumentUtil.getHorizontalOffset());
		Assertions.assertEquals(offsetVert, argumentUtil.getVerticalOffset());
	}

}
